#!/bin/sh
touch data/acme.json
touch data/traefik.log
test -f data/.htpasswd || (which htpasswd && htpasswd data/.htpasswd ${USER})
test -f data/.htpasswd || echo "please create data/.htpasswd manually"
